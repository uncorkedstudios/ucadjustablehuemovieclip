//
//  UCAdjustableHueMovieClip.m
//
//  Created by scott bates on 11/2/11.
//  Uncorked Studios.
//  http://www.uncorkedstudios.com
//
// the core of the main code for doing the matrix transforms necessary to do a Hue shift was taken
// from Apple sample code and adapted to fit into Sparrow's render pipeline.
// 
// The Apple sample code that helped make this possible is:
// https://developer.apple.com/library/ios/#samplecode/GLImageProcessing/Introduction/Intro.html
//

#import "UCAdjustableHueMovieClip.h"


@implementation UCAdjustableHueMovieClip
@synthesize hue;

- (id)initWithFrames:(NSArray *)textures fps:(float)fps
{
    if (textures.count == 0)
        [NSException raise:SP_EXC_INVALID_OPERATION format:@"empty texture array"];
    
    if ((self = [super initWithFrames:textures fps:fps])){
        hue = 1.0;
    }
    return self;
}

- (id)initWithTexture:(SPTexture*)texture
{
    if (!texture) [NSException raise:SP_EXC_INVALID_OPERATION format:@"texture cannot be nil!"];
    if ((self = [super initWithTexture:texture])){
        hue = 1.0;
    }
    return self;
}

// Currently assumes the value being passed is with the range of [0.0 .. 2.0]
// This range maps to a radial based Hue shift where
// 0.0 => -180 degrees
// 1.0 => 0 degrees
// 2.0 => 180 degrees
//

// This is an additive method...  it shifts the current value by the value passed in.
// Use the property if you want to directly set hue to an explicite value.
-(void)adjustHue:(float)val{
    hue+=val;
    if (hue>2.0) {
        hue-=2.0;
    }else if(hue<0){
        hue+=2.0;
    }
}


static void matrixmult(float a[4][4], float b[4][4], float c[4][4])
{
	int x, y;
	float temp[4][4];
    
	for(y=0; y<4; y++)
		for(x=0; x<4; x++)
			temp[y][x] = b[y][0] * a[0][x] + b[y][1] * a[1][x] + b[y][2] * a[2][x] + b[y][3] * a[3][x];
	for(y=0; y<4; y++)
		for(x=0; x<4; x++)
			c[y][x] = temp[y][x];
}


static void xrotatemat(float mat[4][4], float rs, float rc)
{
	mat[0][0] = 1.0;
	mat[0][1] = 0.0;
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
    
	mat[1][0] = 0.0;
	mat[1][1] = rc;
	mat[1][2] = rs;
	mat[1][3] = 0.0;
    
	mat[2][0] = 0.0;
	mat[2][1] = -rs;
	mat[2][2] = rc;
	mat[2][3] = 0.0;
    
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
}


static void yrotatemat(float mat[4][4], float rs, float rc)
{
	mat[0][0] = rc;
	mat[0][1] = 0.0;
	mat[0][2] = -rs;
	mat[0][3] = 0.0;
    
	mat[1][0] = 0.0;
	mat[1][1] = 1.0;
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
    
	mat[2][0] = rs;
	mat[2][1] = 0.0;
	mat[2][2] = rc;
	mat[2][3] = 0.0;
    
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
}


static void zrotatemat(float mat[4][4], float rs, float rc)
{
	mat[0][0] = rc;
	mat[0][1] = rs;
	mat[0][2] = 0.0;
	mat[0][3] = 0.0;
    
	mat[1][0] = -rs;
	mat[1][1] = rc;
	mat[1][2] = 0.0;
	mat[1][3] = 0.0;
    
	mat[2][0] = 0.0;
	mat[2][1] = 0.0;
	mat[2][2] = 1.0;
	mat[2][3] = 0.0;
    
	mat[3][0] = 0.0;
	mat[3][1] = 0.0;
	mat[3][2] = 0.0;
	mat[3][3] = 1.0;
}


static void huematrix(GLfloat mat[4][4], float angle)
{
	float mag, rot[4][4];
	float xrs, xrc;
	float yrs, yrc;
	float zrs, zrc;
    
	// Rotate the grey vector into positive Z
	mag = sqrt(2.0);
	xrs = 1.0/mag;
	xrc = 1.0/mag;
	xrotatemat(mat, xrs, xrc);
	mag = sqrt(3.0);
	yrs = -1.0/mag;
	yrc = sqrt(2.0)/mag;
	yrotatemat(rot, yrs, yrc);
	matrixmult(rot, mat, mat);
    
	// Rotate the hue
	zrs = sin(angle);
	zrc = cos(angle);
	zrotatemat(rot, zrs, zrc);
	matrixmult(rot, mat, mat);
    
	// Rotate the grey vector back into place
	yrotatemat(rot, -yrs, yrc);
	matrixmult(rot,  mat, mat);
	xrotatemat(rot, -xrs, xrc);
	matrixmult(rot,  mat, mat);
}



- (void)render:(SPRenderSupport *)support
{    
    GLubyte half[4] = { 0x80, 0x80, 0x80, 0x80 };
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_UNSIGNED_BYTE, half);
    
    static float texCoords[8];     
    
    GLfloat mat[4][4];
	GLfloat lerp[4] = { 1.0, 1.0, 1.0, 0.5 };
    
	// Color matrix rotation can be expressed as three dot products
	// Each DOT3 needs inputs prescaled to [0.5..1.0]
    
	// Construct 3x3 matrix
	huematrix(mat, (hue-1.0)*M_PI);
    
	// Prescale matrix weights
	mat[0][0] *= 0.5; mat[0][0] += 0.5;
	mat[0][1] *= 0.5; mat[0][1] += 0.5;
	mat[0][2] *= 0.5; mat[0][2] += 0.5;
	mat[0][3] = 1.0;
    
	mat[1][0] *= 0.5; mat[1][0] += 0.5;
	mat[1][1] *= 0.5; mat[1][1] += 0.5;
	mat[1][2] *= 0.5; mat[1][2] += 0.5;
	mat[1][3] = 1.0;
    
	mat[2][0] *= 0.5; mat[2][0] += 0.5;
	mat[2][1] *= 0.5; mat[2][1] += 0.5;
	mat[2][2] *= 0.5; mat[2][2] += 0.5;
	mat[2][3] = 1.0;
    
    
    
	//glClear(GL_COLOR_BUFFER_BIT);
    //glDisable(GL_BLEND);
    
	glBindTexture(GL_TEXTURE_2D, mTexture.textureID);
    //[support bindTexture:mTexture];  
    [mTexture adjustTextureCoordinates:mTexCoords saveAtTarget:texCoords numVertices:4];          
    
    
    SPRectangle *frame = mTexture.frame;
    if (frame)
    {               
        glTranslatef(-frame.x, -frame.y, 0.0f);
        glScalef(mTexture.width / frame.width, mTexture.height / frame.height, 1.0f);        
    }
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    
    //glEnableClientState(GL_COLOR_ARRAY);
    
    glVertexPointer(2, GL_FLOAT, 0, mVertexCoords);
    glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
    //glColorPointer(4, GL_UNSIGNED_BYTE, 0, colors);
    
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
	glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB,      GL_INTERPOLATE);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC0_RGB,         GL_TEXTURE);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC1_RGB,         GL_CONSTANT);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC2_RGB,         GL_CONSTANT);
	glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA,    GL_REPLACE);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC0_ALPHA,       GL_TEXTURE);
	glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR, lerp);
    
    glActiveTexture(GL_TEXTURE1);
	glEnable(GL_TEXTURE_2D);
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
	glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_RGB,      GL_DOT3_RGB);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC0_RGB,         GL_PREVIOUS);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC1_RGB,         GL_PRIMARY_COLOR);
	glTexEnvi(GL_TEXTURE_ENV, GL_COMBINE_ALPHA,    GL_REPLACE);
	glTexEnvi(GL_TEXTURE_ENV, GL_SRC0_ALPHA,       GL_PREVIOUS);
    
    //glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    // Red channel
	glColorMask(1,0,0,0);
	glColor4f(mat[0][0], mat[0][1], mat[0][2], mat[0][3]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	// Green channel
	glColorMask(0,1,0,0);
	glColor4f(mat[1][0], mat[1][1], mat[1][2], mat[1][3]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	// Blue channel
	glColorMask(0,0,1,0);
	glColor4f(mat[2][0], mat[2][1], mat[2][2], mat[2][3]);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    
    // Restore state
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE0);
	glColorMask(1,1,1,0);
    
    
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);    
    
    // Rendering was tested with vertex buffers, too -- but for simple quads and images like these, 
    // the overhead seems to outweigh the benefit. The "glDrawArrays"-approach is faster here.
}


@end
