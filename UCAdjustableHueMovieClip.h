//
//  UCAdjustableHueMovieClip.h
//
//  Created by scott bates on 11/2/11.
//  Uncorked Studios.
//  http://www.uncorkedstudios.com
//
//  Permission is hereby granted, free of charge, to any person
//  obtaining a copy of this software and associated documentation
//  files (the "Software"), to deal in the Software without
//  restriction, including without limitation the rights to use,
//  copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the
//  Software is furnished to do so, subject to the following
//  conditions:
//
//  The above copyright notice and this permission notice shall be
//  included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
//  OTHER DEALINGS IN THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import "SPSprite.h"
#import "SPTexture.h"
#import "SPAnimatable.h"
#import "SPImage.h"

@interface UCAdjustableHueMovieClip : SPMovieClip {
    float hue;
}
@property(readwrite)float hue;

//expects that you pass in a float between 0..2 = -180..180
-(void)adjustHue:(float)val;

//override standard init methods just to establish a default value for hue
- (id)initWithFrames:(NSArray *)textures fps:(float)fps;
- (id)initWithTexture:(SPTexture*)texture;
@end
