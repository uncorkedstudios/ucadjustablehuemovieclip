Adjustable Hue Movie Clip for Sparrow Framework
=============================

For use with the [Sparrow Framework][1]

This is a custom subclass of SPMovieClip that provides methods for applying realtime hue shifting of the images being displayed by the movie clip.  

While this is for an SPMovieClip - the same principle code could be use to also create a subclass of SPImage


How to use it
-------------

Create an instance of the class the same way you would any other SPMovieClip and add it to you stage.  You may then either directly set the hue value or adjust the hue by a value from it's current value.

Currently assumes the value being passed is with the range of [0.0 .. 2.0]

This range maps to a radial based Hue shift where
0.0 => -180 degrees
1.0 => 0 degrees
2.0 => 180 degrees


	[yourInstance hue]; 
 -- returns the current hue adjustment value

	[yourInstance setHue:0.36];  
 -- sets hue to 0.36 which equals a -64.8 degree shift in hue

	[yourInstance adjustHue:0.16];
 -- adds 0.36 to the current hue value. If current value was 1.0 this would result in a value of 1.16  which equals a 28.8 degree shift in hue

 -- the adjustHue method is circular, if the value passed in pushes the hue passed 180 degrees, it will move to the negative -180 and the reverse.

MIT License
-----------
Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


  [1]: http://www.sparrow-framework.org
